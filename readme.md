### NAULET Camille
### HAMELIN Nicolas
### Japhet NAMDEGONE

# DOC TP Architecture logicielle – Créer un framework

## Introduction

Ce projet a pour but de créer un framework basé sur une architecture PIPE/FILTER dédié aux développeurs.

## Getting started
==> Charger le fichier à traiter à la racine

==> Initialiser le fichier de configuration json 

==> Lancer le filtrage fichier : node app.js


## API
==> Initialiser le fichier de configuration json 

|Etape     | Clés   | filtres  | Nom des filtres |

**********************Exemple*********************

{

    "steps" : {

        "1": {

            "filter": "read.js",

            "params" : ["./test.txt"],

            "next": "2"
        },

        "2": {

            "filter": "capitalize.js",

            "next": "3"

        },

        ...

}

==> Filters

** filters/read.js : Ce filter prend en entré un chemin d’un fichier et retourne son contenu

** filters/capitalize.js : Ce filter prend du texte en entré et la passe en majuscule

** filters/reverse.js : Ce filter prend du texte en entré et le retourne inversé (uniquement
les mots, pas les lettres)

** filters/write.js : Ce filter prend du texte et un chemin en entré et écrit le contenu dans un fichier


## Errors

Elles sont déclenchées dans app.js

## Tools
todo