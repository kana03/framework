const filters = './filters/';
const fs = require('fs');

fs.readdir(filters, (err, files) => {
    if (err) {
        console.log("filters n'existe pas");
        return;
    }
    files.forEach(file => {
        console.log(file);
    });
});

fs.readFile("./config-filters.json", "utf8", (err, jsonString) => {
    if (err) {
        console.log("File read failed:", err);
        return;
    }

    const json = JSON.parse(jsonString);
    if (Object.keys(json["steps"]).length === 0) console.log("steps vide");
    else {
        let result = null;
        for (const [stepKey, stepValue] of Object.entries(json["steps"])) {
            if (!stepKey) console.log("clef de l'objet indéfini");
            else {
                if (!"filter" in json["steps"][stepKey]) console.log("l'attribut filter n'existe pas");
                else {
                    const filename = require("./filters/" + json["steps"][stepKey]["filter"]);
                    if (filename.filter == undefined) {
                        console.log("La signature du filtre " + stepValue.filter + " est invalide");
                    } else {
                        console.log("La signature du filtre " + stepValue.filter + " est valide");
                    }
                    if (result === null) result = filename.filter(json["steps"][stepKey]["params"].join());
                    else {
                        if (json["steps"][stepKey]["params"] === undefined) result = filename.filter(result);
                        else result = filename.filter(result, json["steps"][stepKey]["params"].join());
                    }
                }
            }
        }
    }
});
