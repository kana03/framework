const fs = require('fs');

function filter(file_path) {

    try {
        const data = fs.readFileSync(file_path, 'utf8')
        return data;
    } catch (err) {
        console.error('error', err);
    }
}

module.exports = { filter };
