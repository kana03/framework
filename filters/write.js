function filter(text, path) {
    const fs = require('fs');

    fs.writeFile(path, text, function (err) {
        if (err) throw err;
        console.log('Fichier mis à jour !');
    });
}

module.exports = { filter };
